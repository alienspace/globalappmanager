import React, { Component } from 'react';
import {
  Col, Button,
  Alert,
  Modal, FormControl,
} from 'react-bootstrap';
import { browserHistory, Link, } from 'react-router';
import firebase from 'firebase';
import slug from 'slug';
import ReactQuill from "react-quill";

/** COMPONENTS CUSTOM **/
import { APP_CONFIG } from './../../boot.config';
import { TOOLS } from './../../util/tools.global';
import { MasterForm } from './../Form/MasterForm';
// import { Input } from '../Form/Input';
export class AppManager extends Component{
  constructor(props){
    super(props);
    this.state = {
      isLoading : true,
      showModal : false,
      confirmDelete : false,
      action : this.props.action,
      prefix : 'app',
      apps : [],

      uid : this.props.currentUser.uid,
      appID : null,
      title : null,
      slug : null,
      description : '',
      status : true,

      status_response : null,
      message_response : null
    }

    this.checkStatusResponse = this.checkStatusResponse.bind(this);
    this.checkStates = this.checkStates.bind(this);
    this.getApps = this.getApps.bind(this);
    this.resetCurrentState = this.resetCurrentState.bind(this);
    this.renderModal = this.renderModal.bind(this);
    this.requiredInput = TOOLS.requiredInput.bind(this);

    /*** FUNCTIONS AUXILIARES DO FORM ***/
    this.onLoadForm = this.onLoadForm.bind(this);
    this.onSubmitForm = this.onSubmitForm.bind(this);
    this.onUpdateForm = this.onUpdateForm.bind(this);
    this.onChangeForm = this.onChangeForm.bind(this);
    this._onChangeName = this._onChangeName.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.statusInfo = this.statusInfo.bind(this);

    /*** RENDER FUNCTIONS - ACTIONS ***/
    this.actionNull = this.actionNull.bind(this);
    this.actionNew = this.actionNew.bind(this);
    this.actionDelete = this.actionDelete.bind(this);
    this.renderAction = this.renderAction.bind(this);
  }

  componentWillMount(){
    // console.log('componentWillMount');
    if(this.props.action === 'edit' && this.props.appID){
      console.log('componentWillMount if edit ')
      this.setState({ appID : this.props.appID });
    }
  }

  componentDidMount(){
    console.log('User id -> ',this.state.uid);
    let _self=this;
    _self.allApps = firebase.database().ref('apps/');
    _self.apps = this.state.apps;

    /** child_added **/
    _self.allApps.on('child_added', (data) => {
      _self.apps = this.state.apps;
      _self.apps.push(data.val());
      if(data){
        if(_self.apps.length >= 0 ){
          _self.setState({ isLoading : false });
          _self.setState({ apps : _self.apps });
        }
      }
    });
    /** child_added **/

    /** child_changed **/
    _self.allApps.on('child_changed', (data) => {
      if(data){
        let changed = data.val();
        if(_self.apps.length > 0 ){
          console.log('child_changed apps: ', {changed : changed});
          // eslint-disable-next-line
          // _self.apps.map((app) => {
          //   if(app.appID === changed.appID){
          //     app.uid = this.state.uid;
          //     app.appData.title = changed.title;
          //     app.slug = changed.slug;
          //     app.content = changed.content;
          //     app.category = changed.category;
          //     app.typeContent = changed.typeContent;
          //     app.status = changed.status;
          //   }
          // });
          _self.setState({ isLoading : false });
        }else{
          return false;
        }

        _self.setState({ apps : _self.apps });
      }
    });
    /** child_changed **/

    /** child_moved **/
    _self.allApps.on('child_moved', (data) => {
      _self.apps.push(data.val());
      if(data){
        if(_self.apps.length > 0 )
          _self.setState({ isLoading : false });

        _self.setState({ apps : _self.apps });
      }
      console.log('child_moved apps: ', {qtd: _self.apps.length, all: _self.apps});
    });
    /** child_moved **/

    /** child_removed **/
    _self.allApps.on('child_removed', (data) => {
      if(data){
        if(_self.apps.length > 0 ){
          let appRemoved = data.val();
          console.log('res child_removed -> ',appRemoved);
          // eslint-disable-next-line
          _self.apps.map((app, key) => {
            // eslint-disable-next-line
            if(app.appID === appRemoved.appID){
              console.log('Key -> ',key);
              TOOLS.removeArrayItem(_self.apps,key,1);
              console.log('App removeed ',appRemoved.appID);
              _self.setState({ apps : _self.apps, isLoading : false });
            }else{
              console.log('Key -> ',key);
              console.log('App appID ',app.appID);
              console.log('App removeed ',appRemoved.appID);
            }
          });
        }
      }
    });
    /** child_removed **/
  };//componentDidMount();

  componentWillUnmount(){
    console.clear();
  };//componentWillUnmount();

  componentWillReceiveProps(nextProps){
    if(nextProps.action === 'new'){
      return this.resetCurrentState();
    }else if(nextProps.appID || nextProps.action === 'edit'){
      console.log('getApps to ',nextProps);
      return this.getApps(nextProps.appID);
    }
  };//componentWillReceiveProps();

  checkStatusResponse(){
    const closeAlert = (e) => {
      if(e)
        e.preventDefault();
      this.setState({ status_response : null, message_response : null});
    };//closeAlert(e);
    if(this.state.status_response === true){
      setTimeout(() => {
        closeAlert();
      },4000);
      return (<Alert bsStyle={'success'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
    }

    if(this.state.status_response === false){
      setTimeout(() => {
        closeAlert();
      },4000);
      if(this.state.status === 'Rascunho'){
        return (<Alert className={'default'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
      }else if(this.state.status === 'requiredInput'){
        return (<Alert className={'warning'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
      }
      return (<Alert bsStyle={'danger'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
    }

    return null;
  };//checkStatusResponse();

  checkStates(){
    if(this.state.appID)
      this.setState({ appID : this.state.appID })

    console.log('checkStates exit();');
  };//checkStates();

  getApps(appID){
    // console.log('Init getApps ->',appID);
    if( appID ){
      // console.log('Getting appID ->',appID);
      // eslint-disable-next-line
      return this.state.apps.map((app) => {
        // eslint-disable-next-line
        if(app.appID === appID){
          this.setState({
            appID : app.appID,
            title : app.appData.title,
            slug : app.appData.slug,
            description : app.appData.description,
            status: app.appData.status
          });
        }else{
          return false;
        }
      });
      // firebase.database().ref('apps/'+appID).once('value')
      //   .then((res) => {
      //     console.log('getApp 1 res ',res.val().title);
      //   })
      //   .catch((err) => {
      //     console.log('getApp 1 err',err);
      //   })
    }
    this.setState({ isLoading : false });
  };//getApps();

  resetCurrentState(){
    console.log('resetCurrentState();');
    return this.setState({
      showModal : false,
      confirmDelete : false,
      action : this.props.action,

      uid : this.props.currentUser.uid,
      appID : null,
      title : null,
      slug : null,
      description : null,
      status : true,

      status_response : null,
      message_response : null
    });
  };//resetCurrentState();

  renderModal(){
    let
      appTitle = this.state.title;
    const close = () => {
      this.setState({ showModal : false });
    }
    const confirm = () => {
      firebase.database().ref('apps/'+this.state.appID)
        .remove()
        .then(() => {
          this.setState({
            isLoading : false,
            showModal : false,
            confirmDelete : false,
            status : 'Deletado',
            status_response : true,
            message_response : '{ ' + this.state.title + ' } deletado com sucesso!'
          });
        })
        .catch((err) => {
          this.setState({
            isLoading : false,
            showModal : false,
            confirmDelete : true,
            status : 'Error',
            status_response : false,
            message_response : 'Ocorreu uma falha ao tentar deletar { ' + this.state.title + ' } - '+err.message
          });
        });
    }
    return (
      <Modal show={this.state.showModal} onHide={close}>
        <Modal.Header closeButton>
          <Modal.Title>Deseja realmente excluir {appTitle}?</Modal.Title>
        </Modal.Header>
        <Modal.Footer>
          <Link to={'/dashboard/apps'} onClick={close.bind(this)} className={'btn btn-danger'}>Cancelar</Link>
          <Button onClick={confirm.bind(this)} bsStyle={'success'}>Confirmar</Button>
        </Modal.Footer>
      </Modal>
    );
  };//renderModal();

  /*** FUNCTIONS AUXILIARES DO FORM ***/
  onLoadForm(){
    console.log('onLoadForm();');
  };//onLoadForm(e);
  onSubmitForm(e) {
    e.preventDefault();
    let required = document.getElementsByClassName('required');
    if(required.length > 0){
      this.setState({
        isLoading : true,
        status : 'requiredInput',
        status_response : false,
        message_response : 'Por favor, preencha todos os campos corretamente.'
      });
      return false;
    }else{
      const appID = this.refs.appID.value;
      const dataInsert = {
        uid : this.state.uid,
        appID : appID,
        appData: {
          title : this.state.title,
          slug : this.state.slug.toLowerCase(),
          description : this.state.description,
          status: 'Publicado'
        },
        packages: {
          cms: false,
          shop: false
        }
      };
      firebase.database().ref('apps/'+this.state.prefix+appID)
        .set(dataInsert, () => {
          this.setState({
            isLoading : false,
            status_response : true,
            message_response : '{ ' + this.state.title + ' } gravado com sucesso!'
          });
          console.clear();
          browserHistory.push('/dashboard/apps');
        });
    }
    return false;
  };//onSubmitForm(e);
  onUpdateForm(e){
    e.preventDefault();
    let required = document.getElementsByClassName('required');
    if(required.length > 0){
      this.setState({
        isLoading : true,
        status : 'requiredInput',
        status_response : false,
        message_response : 'Por favor, preencha todos os campos corretamente.'
      });
      return false;
    }else{
      const appID = this.state.appID;
      const dataUpdate = {
        uid : this.state.uid,
        appID : appID,
        appData : {
          title : this.state.title,
          slug : this.state.slug.toLowerCase(),
          description : this.state.description,
          status: this.state.status
        },
        packages: {
          cms: false,
          shop: false
        }
      };
      console.log('dataUpdate -> ',dataUpdate);
      firebase.database().ref('apps/'+appID)
        .update(dataUpdate, () => {
          this.setState({
            isLoading : false,
            appID : appID,
            description: '',
            status : 'Publicado',
            status_response : true,
            message_response : '{ ' + this.state.title + ' } gravado com sucesso!'
          });
          console.log('Update successful ',this.state.apps)
          browserHistory.push('/dashboard/apps');
        });
    }
    return false;
  };//onUpdateForm(e);
  onChangeForm(e){
    console.log('onChangeForm',e)
    if(e){
      // eslint-disable-next-line
      this.state.apps.map((app) => {
        if(app.slug === slug(e.target.value)){
          this.setState({
            appID : this.refs.appID.value,
            title : this.refs.title.value,
            slug: slug(this.refs.title.value)+'_2',
            description : this.refs.description.value,
            status: this.state.status
          });
        }else{
          this.setState({
            appID : this.refs.appID.value,
            title : this.refs.title.value,
            slug: slug(this.refs.title.value),
            description : this.refs.description.value,
            status: this.state.status
          });
        }
      });
    }
  };//onChangeForm(e);
  _onChangeName(e){
    if(e){
      this.setState({title: e.target.value, slug: slug(e.target.value), status : 'Publicado'});
    }
  };//onChangeInput(e);
  onChangeDescription(value){
      this.setState({ description : value });
  };//onChangeDescription(value);
  statusInfo(){
    if( this.state.status ){
      if(this.state.status === 'Publicado')
        return (
          <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
            <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'status'} className={ 'label label-success' }>{'Publicado'}</span>
          </label>
        );

      if(this.state.status === 'Rascunho')
        return (
          <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
            <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'status'} className={ 'label label-default' }>{this.state.status || 'Aguardando publicação..'}</span>
          </label>
        );

      if(this.state.status === 'requiredInput')
        return (
          <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
            <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'status'} className={ 'label label-default' }>{'Rascunho'}</span>
          </label>
        );
    }else{
      return (
        <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
          <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'status'} className={ 'label label-default' }>{'Aguardando um título'}</span>
        </label>
      );
    }
  };//statusInfo();



  /*
   *	 RENDER FUNCTIONS - ACTIONS
   *
   *  Funções que renderizam a view do component,
   *  de acordo a ACTION de this.props.action;
   *  Todas as "ACTIONS Functions" definem document.title:
   *  Ex: document.title = APP_CONFIG.PROJECT_NAME + ' | App Name';
   *  { actionNull - actionNew - actionEdit - actionDelete }
   */
  actionNull(){
    /*
     *  Responsável por renderizar a table de Apps
     *  if( this.state.apps.length > 0 )
     */
    document.title = APP_CONFIG.PROJECT_NAME + ' | Apps';
    if( !this.state.isLoading ){
      if( this.state.apps.length > 0 ){
        const renderList = () => {
          return this.state.apps.map((app) => {
            return (
              <tr key={app.appID}>
                <td style={{width:80}}>
                  <Link to={'/dashboard/apps/edit/'+app.appID} className={'btn btn-xs btn-primary'}>
                    <span className={'fa fa-pencil'}></span>
                  </Link>
                  <Link to={'/dashboard/apps/delete/'+app.appID} onClick={() => {this.setState({showModal:true,title:app.appData.title,appID:app.appID})}} className={'btn btn-xs btn-danger'}>
                    <span className={'fa fa-trash'}></span>
                  </Link>
                </td>
                <td style={{width:60}}><small>{app.appID}</small></td>
                <td><small>{app.appData.title}</small></td>
                <td><small>{TOOLS.stripAllTags(app.appData.description).slice(0,59)+'..'}</small></td>
                <td className={'text-center'}><small>{app.appData.status}</small></td>
              </tr>
            );
          });
        }

        return (
          <div className={'table-responsive'}>
            <table className={'table table-hover'}>
              <thead>
                <tr>
                  <th></th>
                  <th>ID</th>
                  <th>TÍTULO</th>
                  <th>DESCRIÇÃO</th>
                  <th className={'text-center'}>STATUS</th>
                </tr>
              </thead>
              <tbody>
                { renderList() }
              </tbody>
            </table>
          </div>
        );
      }else{
        return (
          <Col xs={12} md={12} style={{textAlign:'center',padding:0}}>
            <div className="spinner">
              <div className="double-bounce1"></div>
              <div className="double-bounce2"></div>
            </div>
          </Col>
        );
      }
    }else{
      return (
        <div className={'table-responsive'}>
          <table className={'table table-hover'}>
            <thead>
              <tr>
                <th></th>
                <th>ID</th>
                <th>TÍTULO</th>
                <th>DESCRIÇÃO</th>
                <th className={'text-center'}>STATUS</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td></td>
                <td className={'text-center'}>{'Nenhum registro foi encontrado... '}</td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </div>
      );
    }
  };//actionNull();

  actionNew(){
    /*
     *  Responsável por renderizar o Form para inserção de Apps
     *  if( this.props.action === 'new' )
     */
    document.title = APP_CONFIG.PROJECT_NAME + ' | Novo App';

    return (
      <div>
        <Col xs={12} md={12} className={'no-padding'}>
          <form onChange={this.onChangeForm} onSubmit={this.onSubmitForm}>
            <Col xs={12} md={8} className={'no-padding'}>
              <div className={'input-group col-md-12'}>
                <label>Título do App</label>
                <input
                  type={'hidden'}
                  ref={'appID'}
                  value={TOOLS.uniqueID()}
                  />
                  <input
                    type={'hidden'}
                    ref={'status'}
                    value={'Publicado'}
                    />
                <input
                  className={'form-control required'}
                  onBlur={TOOLS.requiredInput}
                  onChange={this._onChangeName}
                  type="text"
                  ref="title"
                  autoFocus
                  placeholder="Digite o título do app aqui" />
                  <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
                    <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Slug:</span> <span ref={'slug'} className={ this.state.slug ? 'label label-success' : 'label label-default'}>{this.state.slug ? this.state.slug : 'Aguardando um título..'}</span>
                  </label>
              </div>

              <div className={'input-group col-md-12'} style={{marginTop:20}}>
                <ReactQuill
                  theme={'snow'}
                  ref={'description'}
                  value={this.state.description}
                  onChange={this.onChangeDescription}
                  />
              </div>
            </Col>
            <Col xs={12} md={4} className={'no-paddingRight'}>
              {this.statusInfo()}
              <Button
                onClick={this.onSubmitForm}
                className="btn btn-success pull-right no-radius"
                style={{marginTop:30}}
                >Publicar
              </Button>
              <Link
                to={'/dashboard/apps/'}
                className="btn btn-danger pull-right no-radius"
                style={{marginTop:30}}
                >Cancelar
              </Link>
            </Col>
          </form>
        </Col>
      </div>
    );
  };//actionNew();

  actionEdit(){
    /*
     *  Responsável por renderizar o Form para alteração de Apps
     *  if( this.props.action === 'edit' )
     */
    document.title = APP_CONFIG.PROJECT_NAME + ' | Editar App';

    if(this.props.action === 'edit'){
      console.log('action: ',this.props.action);
      if(this.props.appID){
        return (
          <div>
            <Col xs={12} md={12} className={'no-padding'}>
              <MasterForm onLoadForm={this.onLoadForm} onChange={this.onChangeForm} onSubmit={this.onUpdateForm}>
                <Col xs={12} md={8} className={'no-padding'}>
                  <div className={'input-group col-md-12'}>
                    <label>Título do App</label>
                    <FormControl
                      type={'hidden'}
                      ref={'appID'}
                      value={this.props.appID}
                      />
                    <FormControl
                      className={'form-control'}
                      onBlur={TOOLS.requiredInput}
                      onChange={this._onChangeName}
                      type="text"
                      ref={'title'}
                      value={this.state.title}
                      placeholder="Digite o título do app aqui" />
                      <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
                        <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Slug:</span> <span ref={'slug'} className={ this.state.slug ? 'label label-success' : 'label label-default'}>{this.state.slug ? this.state.slug : 'Aguardando um título..'}</span>
                      </label>
                  </div>

                  <div className={'input-group col-md-12'} style={{marginTop:20}}>
                    <ReactQuill
                      theme={'snow'}
                      ref={'description'}
                      value={this.state.description}
                      onChange={this.onChangeDescription}
                      />
                  </div>
                </Col>
                <Col xs={12} md={4} className={'no-paddingRight'}>
                  {this.statusInfo()}
                  <Button
                    onClick={this.onUpdateForm}
                    className="btn btn-success pull-right no-radius"
                    style={{marginTop:30}}
                    >Atualizar
                  </Button>
                  <Link
                    to={'/dashboard/apps/'}
                    className="btn btn-danger pull-right no-radius"
                    style={{marginTop:30}}
                    >Cancelar
                  </Link>
                </Col>
              </MasterForm>
            </Col>
          </div>
        );
      }else{
        return (<h1>Ops.. ocorreu uma falha..</h1>);
      }
      // console.log('appID: ',this.props.appID);
    }
  };//actionEdit();

  actionDelete(){
    /*
     *  Responsável por deletar app
     *  if( this.props.action === 'delete' )
     */
     if(this.props.action === 'delete'){
       if(this.props.appID){
         if(!this.state.confirmDelete){
           return (
             <div>
               {this.actionNull()}
               {this.renderModal()}
             </div>
           );
         }else{
           console.log('confirmDelete!');
           return (
             <div>
               {this.actionNull()}
             </div>
           );
         }
       }else{
         return (
           <h1>actionDelete #OFF</h1>
         );
       }
     }
  };//actionDelete();

  renderAction(){
    /*
     *  Responsável por verificar e chamar ACTIONS.
     *  Ex: if( this.props.action === 'new' ) this.actionNew();
     */
    if( this.props.action === 'new' )
      return this.actionNew();

    if(this.props.action === 'edit')
      return this.actionEdit();

    if(this.props.action === 'delete')
      return this.actionDelete();


    if( !this.props.action )
      return (<div>{this.actionNull()}</div>);
  };//renderAction();

  render(){
    return (
      <div>
        {this.checkStatusResponse()}
        {
          this.renderAction()
        }
      </div>
    );
  }
}
